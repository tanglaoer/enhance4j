package cn.tanglaoer.guava.concurrent;

import com.google.common.util.concurrent.ListenableFutureTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/7
 */
public class ListenableFutureTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ListenableFutureTask<String> listenableFutureTask = ListenableFutureTask.create(() -> Thread.currentThread().getName());
        listenableFutureTask.addListener(() -> System.out.println("after listenableFutureTask"), Executors.newFixedThreadPool(2));

        new Thread(listenableFutureTask).start();
        System.out.println(listenableFutureTask.get());
    }
}

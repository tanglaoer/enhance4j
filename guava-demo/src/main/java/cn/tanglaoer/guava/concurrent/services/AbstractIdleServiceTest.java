package cn.tanglaoer.guava.concurrent.services;

import com.google.common.util.concurrent.AbstractIdleService;
import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.ServiceManager;

import static com.google.common.util.concurrent.MoreExecutors.directExecutor;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/7
 */
public class AbstractIdleServiceTest {
    public static void main(String[] args) {
        TestService testService = new TestService();
        // 添加状态改变之后、调用监听器操作
        testService.addListener(new MyListener(), directExecutor());
        Service service = testService.startAsync();
        System.out.println(service.state());
    }
}

class TestService extends AbstractIdleService {
    @Override
    protected void startUp() throws Exception {

    }

    @Override
    protected void shutDown() throws Exception {

    }
}

class MyListener extends Service.Listener {
    @Override
    public void starting() {
        System.out.println("starting...");
    }
}
package cn.tanglaoer.guava.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.FutureTask;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/7
 */
public class CallableTest {
    public static void main(String[] args) {
        Callable<String> callable = () -> {
            System.out.println(Thread.currentThread().getName());
            return Thread.currentThread().getName();
        };
        FutureTask<String> stringFutureTask = new FutureTask<>(callable);
        new Thread(stringFutureTask).start();
    }
}

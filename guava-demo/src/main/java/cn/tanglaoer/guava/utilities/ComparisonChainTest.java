package cn.tanglaoer.guava.utilities;

import com.google.common.collect.ComparisonChain;
import lombok.Data;
import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/7
 */
public class ComparisonChainTest {
    public static void main(String[] args) {
        ArrayList<TestA> testAS = new ArrayList<>();
        TestA a = new TestA("hello", 320);
        TestA b = new TestA("hello", 43);
        testAS.add(a);
        testAS.add(b);
        System.out.println(testAS);

        testAS.sort((a1, a2) -> ComparisonChain.start()
                .compare(a1.getName(), a2.getName())
                .compare(a1.getAge(), a2.getAge()).result());
        System.out.println(testAS);
    }
}

@Data
class TestA {
    private String name;

    private int age;

    public TestA(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

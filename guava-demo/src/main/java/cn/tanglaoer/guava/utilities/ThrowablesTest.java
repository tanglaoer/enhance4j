package cn.tanglaoer.guava.utilities;

import com.google.common.base.Throwables;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/7
 */
public class ThrowablesTest {
    public static void main(String[] args) {
        try {
            testB();
        } catch (BException e) {
            // 获取根部异常
            // Throwable rootCause = Throwables.getRootCause(e);
            // rootCause.printStackTrace();
            e.printStackTrace();
        }
    }

    public static void testB() throws BException {
        try {
            test();
        } catch (A e) {
            throw new BException(e);
        }
    }
    public static void test() throws A {
        throw new A("hello");
    }
}

class BException extends Exception {
    public BException(Throwable cause) {
        super(cause);
    }
}

class A extends Exception {
    public A(String message) {
        super(message);
    }
}

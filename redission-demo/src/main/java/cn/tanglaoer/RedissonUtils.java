package cn.tanglaoer;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/6
 */
public class RedissonUtils {
    private static final String HOST = "redis://192.168.56.101:6381";

    private static final Config config;

    static {
        // 设置redis配置信息
        config = new Config();
        config.useSingleServer().setAddress(HOST).setDatabase(0);
    }

    private static class RedissonHolder {
        private static final RedissonClient redissonClient = Redisson.create(config);
    }

    public static RedissonClient getRedissonClient() {
        return RedissonHolder.redissonClient;
    }
}

package cn.tanglaoer;

import org.junit.Test;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.util.concurrent.TimeUnit;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/6
 */
public class SingleLock {
    public static RedissonClient client = RedissonUtils.getRedissonClient();

    public static void main(String[] args) {
        testLock();
        // testLockTime();
        // testTryLock();
    }

    /**
     * watchDog:自动续期、默认是30秒、剩下20秒的时候自动续期为三十秒、直到程序解锁、如果程序挂掉、三十秒后、锁自动释放
     */
    public static void testLock() {
        RLock lock = client.getLock("myLock");
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "上锁成功");
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName() + "业务执行完成");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println(Thread.currentThread().getName() + "释放锁");
            // 这里不释放锁、看锁会不会释放、检测watchDog的存在
            // lock.unlock();
        }
    }

    /**
     * lock(long, timeout);
     * 时间不好估算、解锁的时候抛出异常
     */
    public static void testLockTime() {
        new Thread(getLockTime(), "thread1").start();
        new Thread(getLockTime(), "thread2").start();
    }

    /**
     * 不断循环尝试获取锁、耗费资源
     */
    public static void testTryLock() {
        new Thread(getTryLock(), "thread1").start();
        new Thread(getTryLock(), "thread2").start();
    }


    private static Runnable getTryLock() {
        return () -> {
            RLock lock = client.getLock("myLock");
            while (true) {
                boolean b = false;
                try {
                    b = lock.tryLock(5, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                if (!b) {
                    continue;
                }
                try {
                    System.out.println(Thread.currentThread().getName() + ":" + System.currentTimeMillis() + "上锁成功");
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + ":" + "业务执行完成");
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                } finally {
                    // 解锁失败、抛出异常
                    System.out.println(Thread.currentThread().getName() + ":释放锁");
                    lock.unlock();
                }
                break;
            }
        };
    }


    private static Runnable getLockTime() {
        return () -> {
            RLock lock = client.getLock("myLock");
            lock.lock(5, TimeUnit.SECONDS);
            try {
                System.out.println(Thread.currentThread().getName() + ":" + System.currentTimeMillis() + "上锁成功");
                Thread.sleep(10 * 1000);
                System.out.println(Thread.currentThread().getName() + ":" + "业务执行完成");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                // 解锁失败、抛出异常
                lock.unlock();
            }
        };
    }
}

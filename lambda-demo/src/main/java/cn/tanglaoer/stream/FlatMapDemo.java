package cn.tanglaoer.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author：tangkaishou
 * @date： 2024/7/2
 */
public class FlatMapDemo {
    public static void main(String[] args) {
        List<Integer> collect = Stream.of(Arrays.asList(1, 2), Arrays.asList(3, 4))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        System.out.println(collect);
    }
}

package cn.tanglaoer.demo;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.concurrent.Future;

/**
 * @author kaishou
 * @description 功能描述
 * @create 2024/1/28 16:17
 */
public class ProductDemo1 {
    public static final String BROKER_LIST = "8.130.67.60:9092";
    public static final String TOPIC = "quickstart-events";
    public static void main(String[] args) {
        try {
            Properties properties = new Properties();
            properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            properties.put("bootstrap.servers", BROKER_LIST);
            KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(TOPIC, "quickest");
            try {
                for (int i = 0; i < 10000; i++) {
                    System.out.println(i);
                    RecordMetadata recordMetadata = producer.send(record).get();
                    System.out.println(recordMetadata.offset());
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            producer.close();
        }catch (Exception e) {
           e.printStackTrace();
        }
    }
}

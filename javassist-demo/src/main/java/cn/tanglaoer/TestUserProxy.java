package cn.tanglaoer;

import javassist.util.proxy.ProxyFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/5
 */
public class TestUserProxy {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ProxyFactory enhancer = new ProxyFactory();
        enhancer.setSuperclass(User.class);
        enhancer.setHandler(new TargetMethodInteger());
        User user = (User) enhancer.create(null, null);
        System.out.println(user);

    }
}

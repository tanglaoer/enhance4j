package cn.tanglaoer;

import javassist.util.proxy.MethodHandler;

import java.lang.reflect.Method;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/5
 */
public class TargetMethodInteger implements MethodHandler {
    @Override
    public Object invoke(Object o, Method method, Method methodProxy, Object[] objects) throws Throwable {
        System.out.println("before method " + method); // 原生的方法、但是不能执行这个、会报错
        System.out.println("before method1 " + methodProxy);
        return methodProxy.invoke(o, objects);

    }
}

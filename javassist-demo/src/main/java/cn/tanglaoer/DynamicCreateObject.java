package cn.tanglaoer;

import javassist.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Hello world!
 */
public class DynamicCreateObject {
    public static void main(String[] args) throws NotFoundException, CannotCompileException, InstantiationException, IllegalAccessException, InvocationTargetException {
        ClassPool pool = ClassPool.getDefault();
        CtClass userCtClazz = pool.makeClass("cn.tanglaoer.entity.User");
        // 创建name属性
        CtField nameField = new CtField(pool.get("java.lang.String"), "name", userCtClazz);
        userCtClazz.addField(nameField);
        // 创建name的set方法
        CtMethod setMethod = CtNewMethod.make("public void setName(String name){this.name = name;}", userCtClazz);
        userCtClazz.addMethod(setMethod);

        // 创建sayHello方法
        CtMethod sayHello = CtNewMethod.make("public String sayHello() { return \"Hello, I am \" + this.name ;}", userCtClazz);
        userCtClazz.addMethod(sayHello);

        // 生成class对象
        Class<?> userClazz = userCtClazz.toClass();
        Object user = userClazz.newInstance();

        Method[] methods = userClazz.getMethods();
        for (Method method : methods) {
            if (method.getName().equals("setName")) {
                method.invoke(user, "tangaloer");
            }
        }

        for (Method method : methods) {
            if (method.getName().equals("sayHello")) {
                String result = (String) method.invoke(user);
                System.out.println(result);
            }
        }
    }
}

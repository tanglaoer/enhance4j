package cn.tanglaoer.completableFuture;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/11
 */
public class CreateDemo {

    /**
     * 创建一个无消耗值（无输入值）、无返回值的异步子任务
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void runAsyncDemo() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            System.out.println("hello world");
        });
        future.get();
    }

    /**
     * 创建一个无输入值、有返回值的异步子任务
     */
    @Test
    public void supplyAsyncDemo() throws ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            long start = System.currentTimeMillis();
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return System.currentTimeMillis() - start;
        });
        Long time = future.get(2, TimeUnit.SECONDS);
        System.out.println(time);
    }


    @Test
    public void whenCompleteDemo() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            System.out.println("抛出异常");
            throw new RuntimeException();
        });

        future.whenComplete(new BiConsumer<Void, Throwable>() {
            @Override
            public void accept(Void unused, Throwable throwable) {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("执行完成");
            }
        });

        future.exceptionally(new Function<Throwable, Void>() {
            @Override
            public Void apply(Throwable throwable) {
                System.out.println("exceptionally");
                return null;
            }
        });

        future.get();
    }

}

package cn.tanglaoer.clazz;

import java.lang.reflect.Method;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/4
 */
public class DemoClazz {

    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) throws NoSuchMethodException {
        DemoClazz demoClazz = new DemoClazz();
        Method toString = demoClazz.getClass().getMethod("toString");
        // Method toString = demoClazz.getClass().getMethod("getName");
        Class<?> declaringClass = toString.getDeclaringClass();
        System.out.println(declaringClass);
    }
}

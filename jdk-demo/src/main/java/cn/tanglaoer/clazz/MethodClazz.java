package cn.tanglaoer.clazz;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/4
 */
public class MethodClazz {
    public void testParam(String param, @MyAnn(value = "hello world")  String noNull) {
        System.out.println("hello");
    }

    public static void main(String[] args) throws NoSuchMethodException {
        MethodClazz methodClazz = new MethodClazz();
        Method method = methodClazz.getClass().getMethod("testParam", String.class, String.class);
        System.out.println(method);
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        int length = parameterAnnotations.length;
        System.out.println(length);
        for (int i = 0; i < length; i++) {
            System.out.println(i);
            for (Annotation annotation : parameterAnnotations[i]) {
                if (annotation instanceof MyAnn) {
                    System.out.println("hint");
                }
                System.out.println(((MyAnn)annotation).value());
            }
        }

        List<String> collect = Arrays.stream(method.getParameters()).map(Parameter::getName).collect(Collectors.toList());
        System.out.println(collect);
    }
}

package cn.tanglaoer.interfaces;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/6
 */
public class InterfaceDemo {

}

interface Interface1 {
    default void work() {
        System.out.println("Interface1");
    }

    default void test() {
        System.out.println("test");
    }
}

interface Interface2 {
    default void work() {
        System.out.println("Interface2");
    }

    default void doSomething() {
        System.out.println("doSomething");
    }
}

/**
 * 多个接口有相同的默认方法、必须重写
 */
class ImplInterface implements Interface1, Interface2 {
    @Override
    public void work() {
        Interface1.super.work();
    }
}
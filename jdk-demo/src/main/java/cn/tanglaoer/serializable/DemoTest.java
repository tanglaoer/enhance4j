package cn.tanglaoer.serializable;

import java.io.*;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/20
 */
public class DemoTest implements Serializable /*implements Externalizable*/ {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DemoTest{" +
                "name='" + name + '\'' +
                '}';
    }


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        DemoTest demoTest = new DemoTest();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(demoTest);
        objectOutputStream.flush();
        byte[] bytes = byteArrayOutputStream.toByteArray();

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        DemoTest demoTest1 = (DemoTest) objectInputStream.readObject();
        System.out.println(demoTest1);
    }
}

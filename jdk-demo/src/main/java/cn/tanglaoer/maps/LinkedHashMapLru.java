package cn.tanglaoer.maps;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/1
 */
public class LinkedHashMapLru {
    private Object eldestKey;

    private Map<Object, Object> keyMap;

    public Map<Object, Object> creatLRUMap(int size) {
        keyMap = new LinkedHashMap<Object, Object>(5, .75F, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Object, Object> eldest) {
                boolean tooBig = size() > size;
                if (tooBig) {
                    // 最久未访问的元素
                    eldestKey = eldest.getKey();
                }
                return tooBig;
            }
        };
        return keyMap;
    }

    public static void main(String[] args) {
        LinkedHashMapLru linkedHashMapLru = new LinkedHashMapLru();
        Map<Object, Object> map = linkedHashMapLru.creatLRUMap(4);
        map.put("string", "string");
        map.put("string1", "string");
        map.put("string2", "string");
        map.put("string3", "string");
        map.put("string4", "string");
        map.put("string5", "string");

        System.out.println(map);
        System.out.println(linkedHashMapLru.eldestKey);
    }
}

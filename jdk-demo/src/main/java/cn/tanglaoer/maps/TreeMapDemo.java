package cn.tanglaoer.maps;

import java.util.Comparator;
import java.util.TreeMap;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/21
 */
public class TreeMapDemo {
    public static void main(String[] args) {
        TreeMap<Person, String> treeMap = new TreeMap<>(Comparator.comparingInt(Person::getAge));
        treeMap.put(new Person(3), "person1");
        treeMap.put(new Person(18), "person2");
        treeMap.put(new Person(35), "person3");
        treeMap.put(new Person(16), "person4");
        treeMap.forEach((key, value) -> System.out.println(value));
    }
}

class Person {
    private Integer age;

    public Person(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}

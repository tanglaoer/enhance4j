package cn.tanglaoer.arrays;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/20
 */
public class ArraysDemo {
    public static void main(String[] args) {
        Pather[] arrays = new Pather[2];
        arrays[0] = new Sub();

        for (int i = 0; i < arrays.length; i++) {
            System.out.println(arrays[i]);
        }
    }
}

class Pather {

}

class Sub extends Pather {

}
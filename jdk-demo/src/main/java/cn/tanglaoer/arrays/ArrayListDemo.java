package cn.tanglaoer.arrays;

import java.util.ArrayList;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/24
 */
public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<String> demos = new ArrayList<>(16);
        demos.add("hello");
        demos.add("world");
        demos.add("pss");
        demos.add(1, "hello world");
        demos.remove(0);
        for (String demo : demos) {
            System.out.println(demo);
        }
    }
}

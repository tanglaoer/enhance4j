package cn.tanglaoer.lock;

import java.lang.reflect.Field;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/13
 */
public class AQSDemo {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Demo demo = new Demo("hello");
        Field name = demo.getClass().getDeclaredField("name");
        name.setAccessible(true);
        System.out.println(name.get(demo));
    }
}

class Demo {
    private String name;

    public Demo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
package cn.tanglaoer.collections;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/24
 */
public class LinkHashMapDemo {
    public static void main(String[] args) {
        // 抛出异常、遍历集合的时候、不能修改集合、true表示安排访问顺序排序、get方法会改变集合内容。所以不能再迭代器中使用get
        // LinkedHashMap<String, String> demos = new LinkedHashMap<>(2, 0.75f, true);
        LinkedHashMap<String, String> demos = new LinkedHashMap<>(2);
        demos.put("foo", "bar");
        demos.put("bar", "baz");

        Iterator<String> iterator = demos.keySet().iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(demos.get(next));
        }
    }
}

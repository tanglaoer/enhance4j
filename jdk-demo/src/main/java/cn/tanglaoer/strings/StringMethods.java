package cn.tanglaoer.strings;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/5
 */
public class StringMethods {
    public static void main(String[] args) throws NoSuchMethodException {
        String s = "helloworld";
        String s1 = s.toUpperCase(Locale.ENGLISH);
        System.out.println(s1);


        StringMethods stringMethods = new StringMethods();
        Method createList = stringMethods.getClass().getMethod("createList", String.class);
        System.out.println(createList.getReturnType());
        System.out.println(createList.getGenericReturnType() + ""  + createList.getGenericReturnType().getClass());
        ParameterizedType returnType = (ParameterizedType) createList.getGenericReturnType();
        Type[] typeArgs = returnType.getActualTypeArguments();
        for (Type typeArg : typeArgs) {
            System.out.println(typeArg);
            System.out.println(typeArg.getClass());
        }

        System.out.println(Arrays.stream(createList.getParameters()).map(Parameter::getName).collect(Collectors.toList()));
    }

    public List<List<StringMethods>> createList(String name) {
        return Collections.emptyList();
    }
}

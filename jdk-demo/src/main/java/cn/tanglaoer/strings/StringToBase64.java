package cn.tanglaoer.strings;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2024/5/1
 */
public class StringToBase64 {
    public static void main(String[] args) {
        String secret = "tanglaoer";
        String s = Base64.getEncoder().encodeToString(secret.getBytes(StandardCharsets.UTF_8));
        System.out.println(s);
    }
}

package cn.tanglaoer.concurrent;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/9/15
 */
public class ConcurrentMapDemo {
    private static ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>(){
        {
            put("key", "value");
        }
    };

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            if (map.get("key") != null) {
                System.out.println(Thread.currentThread().getName());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                map.put("demo", "thread1");
                map.remove("key");
            }
        }, "thread1").start();

        new Thread(() -> {
            if (map.get("key") != null) {
                System.out.println(Thread.currentThread().getName());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                map.put("demo", "thread2");
            }
        }, "thread2").start();

        Thread.sleep(2000L);
        System.out.println(map.get("demo"));
    }
}

package cn.tanglaoer.genericity;

import java.util.List;

/**
 * @author kaishou
 * @description 功能描述
 * @create 2024/6/27 23:07
 */
public class TestDemo<T> {
    public <T> void addElement(List<? super T> list, T element) {
        list.add(element);
    }
}

package cn.tanglaoer.genericity;

/**
 * @author kaishou
 * @description 功能描述
 * @create 2024/6/27 23:06
 */
public class Sub extends Parent {
    private String age;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}

package cn.tanglaoer.genericity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kaishou
 * @description 功能描述
 * @create 2024/6/27 23:06
 */
public class TestMain {
    public static void main(String[] args) {
        List<Parent> parents = new ArrayList<Parent>();
        Sub sub = new Sub();
        TestDemo<Sub> subTestDemo = new TestDemo<Sub>();
        subTestDemo.addElement(parents, sub);

        for (Parent parent : parents) {
            System.out.println(parent.getParent());
        }
    }

}

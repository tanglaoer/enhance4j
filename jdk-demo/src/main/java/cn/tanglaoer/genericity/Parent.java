package cn.tanglaoer.genericity;

/**
 * @author kaishou
 * @description 功能描述
 * @create 2024/6/27 23:06
 */
public class Parent {
    private String parent;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
}

package cn.tanglaoer;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/2
 */
public class TargetInterceptor implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("before invoke");
        Object result = methodProxy.invokeSuper(o, objects);
        System.out.println("after invoke");
        return result;
    }
}

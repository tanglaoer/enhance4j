package cn.tanglaoer;

import net.sf.cglib.proxy.Enhancer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Enhancer enhancer = new Enhancer();
        enhancer.setCallback(new TargetInterceptor());
        // cglib是通过给被代理类创建一个子类
        enhancer.setSuperclass(User.class);
        User user = (User) enhancer.create();

        System.out.println(user);
    }
}

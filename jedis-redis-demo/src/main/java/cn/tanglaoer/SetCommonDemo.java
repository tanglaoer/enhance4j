package cn.tanglaoer;

import cn.hutool.core.util.RandomUtil;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.ScanParams;
import redis.clients.jedis.resps.ScanResult;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * set数据结构的用法
 *
 */
public class SetCommonDemo {
    /**
     * 创建数据
     */
    @Test
    public void createData() {
        Jedis jedis = RedisUtils.getJedis();
        for (int i = 0; i < 20; i++) {
            jedis.sadd("random", RandomUtil.randomString(3));
        }
    }

    /**
     * 获取所有的数据、生产环境慎用、会造成这个key的阻塞
     * [DT1, Vco, FKI, 1uJ, B3x, dik, FTW, x1F, ieh, ZQF, dfp, 6Oi, q8W, Vv6, Q2w, rNr, BWm, VGl, OyD, Re0]
     */
    @Test
    public void smembers() {
        Jedis jedis = RedisUtils.getJedis();
        Set<String> random = jedis.smembers("random");
        System.out.println(random);
        System.out.println(random.size());
    }

    /**
     * 增量遍历（很重要）生产环境都是用这个命令来遍历
     */
    @Test
    public void sscan() {
        Jedis jedis = RedisUtils.getJedis();
        String cursor = "0";
        while (true) {
            // KTN
            ScanResult<String> word = jedis.sscan("random", cursor, new ScanParams().count(30).match("F*"));
            System.out.println(word.getCursor());
            cursor = word.getCursor();
            List<String> result = word.getResult();
            System.out.println(result);
            if (cursor.equals("0")) {
                break;
            }
        }
    }

    @Test
    public void sismember() {
        Jedis jedis = RedisUtils.getJedis();
        System.out.println(jedis.sismember("random", "FTW"));
    }
}

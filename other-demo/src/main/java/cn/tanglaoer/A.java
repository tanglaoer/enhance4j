package cn.tanglaoer;

import java.util.HashSet;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/5
 */
public class A {
    public static void main(String[] args) {
        C b = new C();
        HashSet<Class<?>> objects = new HashSet<>();
        Class<?> aClass = b.getClass();
        while (aClass != null) {
            for (Class<?> anInterface : aClass.getInterfaces()) {
                System.out.println(aClass + "==" + anInterface);
                objects.add(anInterface);
            }
            aClass = aClass.getSuperclass();
        }
    }
}

interface BInterface {

}

interface CInterface {

}

class B implements BInterface {

}

class C extends B  implements CInterface {

}

package cn.tanglaoer;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/4
 */
public class BuilderDesign {
    private String name;

    private BuilderDesign(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static class Builder {
        private String name;

        public Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public BuilderDesign build() {
            return new BuilderDesign(name);
        }
    }
}

package cn.tanglaoer;

import org.junit.Test;

/**
 * @author <a href="https://github.com/TangLaoEr">tks</a>
 * @Date 2023/10/4
 */
public class BuilderDesignTest {
    @Test
    public void test() {
        BuilderDesign helloWorld = new BuilderDesign.Builder().name("hello world").build();
        System.out.println(helloWorld.getName());
    }
}
